/* Twonkie v2.1
 * Lower half 3D printable case
 * by XenGi 2023
 *
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */

include <lib.scad>

$fn = 64;


difference() {
  hull() {
    translate([wall_thickness,wall_thickness,0]) cylinder(h=pcb_height/2 + wall_thickness/2, r=wall_thickness);
    translate([wall_thickness + pcb_width,wall_thickness,0]) cylinder(h=pcb_height/2 + wall_thickness/2, r=wall_thickness);
    translate([wall_thickness,wall_thickness + pcb_length,0]) cylinder(h=pcb_height/2 + wall_thickness/2, r=wall_thickness);
    translate([wall_thickness + pcb_width,wall_thickness + pcb_length,0]) cylinder(h=pcb_height/2 + wall_thickness/2, r=wall_thickness);
  }
  
  // inner hole
  translate([wall_thickness, wall_thickness, wall_thickness/2]) cube([pcb_width, pcb_length, pcb_height/2+dummy]);
  
  // TODO: hole for LED
  translate([wall_thickness + pcb_width - 2.5,wall_thickness + 2,0]) cylinder(h=wall_thickness/2+dummy, r1=2, r2=1);

  // hole for USB-C female (left)
  translate([0,wall_thickness + pcb_length - 2 - usb_c_female_height/2,wall_thickness/2 + pcb_height/2]) hull() {
    rotate([0,90,0]) cylinder(h=wall_thickness, d=usb_c_female_height);
    translate([0,-usb_c_female_width + usb_c_female_height - dummy,0]) rotate([0,90,0]) cylinder(h=wall_thickness, d=usb_c_female_height);
  }
  
  // hole for USB-C male (right)
  translate([wall_thickness + pcb_width,wall_thickness + pcb_length - 2 - usb_c_male_height/2,wall_thickness/2 + pcb_height/2]) hull() {
    rotate([0,90,0]) cylinder(h=wall_thickness, d=usb_c_male_height);
    translate([0,-usb_c_male_width + usb_c_male_height - dummy,0]) rotate([0,90,0]) cylinder(h=wall_thickness, d=usb_c_male_height);
  }
}

// snap fits
hull() {
  translate([wall_thickness + pcb_width/2 - 1.5,wall_thickness + pcb_length,wall_thickness/2 + pcb_height/2]) rotate([270,0,0]) cylinder(h=wall_thickness/2, r=1);
  translate([wall_thickness + pcb_width/2 + 1.5,wall_thickness + pcb_length,wall_thickness/2 + pcb_height/2]) rotate([270,0,0]) cylinder(h=wall_thickness/2, r=1);
  translate([wall_thickness + pcb_width/2,wall_thickness + pcb_length,wall_thickness/2 + pcb_height/2 + 1]) rotate([270,0,0]) cylinder(h=wall_thickness/2, r=1);
}
translate([wall_thickness + pcb_width/2, wall_thickness + pcb_length + wall_thickness/2 - 0.2,wall_thickness/2 + pcb_height/2 + 1]) sphere(r=0.5);

hull() {
  translate([wall_thickness + pcb_width/2 - 1.5,wall_thickness/2,wall_thickness/2 + pcb_height/2]) rotate([270,0,0]) cylinder(h=wall_thickness/2, r=1);
  translate([wall_thickness + pcb_width/2 + 1.5,wall_thickness/2,wall_thickness/2 + pcb_height/2]) rotate([270,0,0]) cylinder(h=wall_thickness/2, r=1);
  translate([wall_thickness + pcb_width/2,wall_thickness/2,wall_thickness/2 + pcb_height/2 + 1]) rotate([270,0,0]) cylinder(h=wall_thickness/2, r=1);
}
translate([wall_thickness + pcb_width/2, wall_thickness/2 + 0.2,wall_thickness/2 + pcb_height/2 + 1]) sphere(r=0.5);

// USB-C male outer case
difference() {
  hull() {
    translate([wall_thickness*2 + pcb_width,wall_thickness + pcb_length - 2 - usb_c_male_height/2,wall_thickness/2 + pcb_height/2]) {
      rotate([0,90,0]) cylinder(h=2, d=usb_c_male_height + wall_thickness);
      translate([0,-usb_c_male_width + usb_c_male_height - dummy,0]) rotate([0,90,0]) cylinder(h=2, d=usb_c_male_height + wall_thickness);
    }
    translate([wall_thickness + pcb_width,wall_thickness + pcb_length - 2 - 2*usb_c_male_height - usb_c_male_height,0]) cube([wall_thickness, usb_c_male_width-2, wall_thickness]);
  }
  translate([wall_thickness*2 + pcb_width,wall_thickness + pcb_length - 2 - usb_c_male_height/2,wall_thickness/2 + pcb_height/2]) hull() {
    translate([-2,0,0]) rotate([0,90,0]) cylinder(h=4, d=usb_c_male_height);
    translate([-2,-usb_c_male_width + usb_c_male_height - dummy,0]) rotate([0,90,0]) cylinder(h=4, d=usb_c_male_height);
  }
  translate([wall_thickness*2 + pcb_width-2,0,wall_thickness/2 + pcb_height/2]) cube([4, pcb_length+wall_thickness,(usb_c_male_height + wall_thickness)/2]);
}