/* Twonkie v2.1
 * Lower half 3D printable case
 * by XenGi 2023
 *
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */

include <lib.scad>

$fn = 64;

translate([0,0,wall_thickness/4]) cylinder(h=wall_thickness/2, r1=2, r2=1);
cylinder(h=wall_thickness/4, r1=1.8, r2=2);
