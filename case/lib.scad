/* Twonkie v2.1
 * Lower half 3D printable case
 * by XenGi 2023
 *
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 */

pcb_width = 25.5;
pcb_length = 25.5;
pcb_height = 5;
wall_thickness = 1.5;

usb_c_male_width = 8.5;
usb_c_male_height = 2.5;

usb_c_female_width = 9;
usb_c_female_height = 3.5;

usb_micro_width = 8;

dummy = 0.1;
